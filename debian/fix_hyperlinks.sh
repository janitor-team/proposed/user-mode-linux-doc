#!/bin/sh

for i in *.html *.png *.gif  ; do
	echo "Fixing links for $i";
	sed -i -e "s/http:\/\/user-mode-linux\.sourceforge\.net\/$i/$i/g" \
		debian/user-mode-linux-doc/usr/share/doc/user-mode-linux-doc/html/*.html;

	# These fix links where remote logo was being fetched
	sed -i -e "s|http://sourceforge.net/sflogo.php?group_id=429||g" \
		debian/user-mode-linux-doc/usr/share/doc/user-mode-linux-doc/html/*.html;

	sed -i -e "s|https://www.paypal.com/en_us/i/btn/x-click-but21.gif||g" \
		debian/user-mode-linux-doc/usr/share/doc/user-mode-linux-doc/html/*.html;

	sed -i -e "s|http://www.google.com/logos/logo_40wht.gif||g" \
		debian/user-mode-linux-doc/usr/share/doc/user-mode-linux-doc/html/*.html;
done

sed -i -e "s|http://user-mode-linux.sourceforge.net/UserModeLinux-HOWTO.txt|UserModeLinux-HOWTO.txt.gz|g" \
	debian/user-mode-linux-doc/usr/share/doc/user-mode-linux-doc/html/*.html;
