user-mode-linux-doc (20060501+repack0-1) unstable; urgency=medium

  * Bump the revision so that we can repack a new orig tarball because the old
    one has different checksum

 -- Ritesh Raj Sarraf <rrs@debian.org>  Tue, 07 Sep 2021 17:38:42 +0530

user-mode-linux-doc (20060501-5) unstable; urgency=medium

  * This documentation is effectively unmaintained upstream. This will
    probably be the last upload of this package, given that the shipped
    documentation may still be of use. The latest documentation is now
    maintained in the Linux Kernel repository.
  * Also add a NEWS entry for the status of the documentation
  * Add debian/gbp.conf file

 -- Ritesh Raj Sarraf <rrs@debian.org>  Fri, 03 Sep 2021 21:07:13 +0530

user-mode-linux-doc (20060501-4) unstable; urgency=low

  * Trim trailing whitespace.
  * Upgrade to newer source format 3.0 (quilt). (Closes: #993419)
  * Bump debhelper from deprecated 9 to 12.
  * Set debhelper-compat version in Build-Depends.
  * Use canonical URL in Vcs-Browser.

 -- Debian Janitor <janitor@jelmer.uk>  Tue, 28 Jul 2020 03:59:25 -0000

user-mode-linux-doc (20060501-3) unstable; urgency=medium

  * Switch packaging to Salsa
  * Switch Maintainer email address (Closes: #899721)
  * Use tracker as maintainer email address

 -- Ritesh Raj Sarraf <rrs@debian.org>  Tue, 12 Jun 2018 10:15:11 +0545

user-mode-linux-doc (20060501-2) unstable; urgency=medium

  * Add myself to Uploaders and drop previous inactive uploaders
  * Add Vcs headers
  * Add recommended build targets
  * Add homepage field
  * Drop sf logo from local documentation
  * Add doc-base section for documentation
    (Closes: #514668, #817710)

 -- Ritesh Raj Sarraf <rrs@debian.org>  Wed, 20 Jul 2016 16:16:42 +0530

user-mode-linux-doc (20060501-1) unstable; urgency=low

  * Take this package under the UML Maintainers umbrella.
  * Update dh_compat and Standard.
  * Downloaded pages from sf.net as the CVS doesn't seem
    accessible. (Closes: #188466)
  * Hopefully fixed all relative links using sed
    appropriately. (Closes: #193162)

 -- Mattia Dongili <malattia@debian.org>  Mon, 01 May 2006 17:23:44 +0200

user-mode-linux-doc (20020523-1.1) unstable; urgency=low

  * Non-maintainer upload.
  * No changes, just rebuilt with a current debhelper to complete the /usr/doc
    transition. Closes: #322805

 -- Joey Hess <joeyh@debian.org>  Tue, 10 Jan 2006 20:51:16 -0500

user-mode-linux-doc (20020523-1) unstable; urgency=low

  * Pull latest stuff from CVS
  * Regenerate HTML using relative links, and correct paths for images
    (Closes: #145964)

 -- Matt Zimmerman <mdz@debian.org>  Thu, 23 May 2002 23:48:06 -0400

user-mode-linux-doc (20020320-2) unstable; urgency=low

  * Fix paths to the html documents in our doc-base entry
    (Closes: #141062)

 -- Matt Zimmerman <mdz@debian.org>  Wed,  3 Apr 2002 21:29:46 -0500

user-mode-linux-doc (20020320-1) unstable; urgency=low

  * New upstream release.
  * Include PNG images (Closes: #139049)

 -- Matt Zimmerman <mdz@debian.org>  Wed, 20 Mar 2002 22:59:30 -0500

user-mode-linux-doc (20020227-1) unstable; urgency=low

  * Initial release.
  * Provides local copy of web documentation (Closes: #129153)
  * Now includes a proper copyright notice

 -- Matt Zimmerman <mdz@debian.org>  Wed, 27 Feb 2002 00:41:16 -0500
